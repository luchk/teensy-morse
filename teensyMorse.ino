
const int led = LED_BUILTIN;

void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(14100000);
}
long lasttime = 0;

void morse(char toMorse) {

  if(toMorse == 32){
    spaceBtwWords();
    serialprnt(toMorse);
  }

  if(toMorse == 65 || toMorse == 97){
    dot();
    longg();
    SBL();
    serialprnt(toMorse);

  }

  if(toMorse == 66 || toMorse == 98){
    longg();
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 67 || toMorse == 99){
    longg();
    dot();
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 68 || toMorse == 100){
    longg();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 69 || toMorse == 101){
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 70 || toMorse == 102){
    longg();
    longg();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 71 || toMorse == 103){
    longg();
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 72 || toMorse == 104){
    dot();
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 73 || toMorse == 105){
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 74 || toMorse == 106){
    dot();
    longg();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 75 || toMorse == 107){
    longg();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 76 || toMorse == 108){
    dot();
    longg();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 77 || toMorse == 109){
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 78 || toMorse == 110){
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 79 || toMorse == 111){
    longg();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 80 || toMorse == 112){
    dot();
    longg();
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 81 || toMorse == 113){
    longg();
    longg();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 82 || toMorse == 114){
    dot();
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 83 || toMorse == 115){
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 84 || toMorse == 116){
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 85 || toMorse == 117){
    dot();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 86 || toMorse == 118){
    dot();
    dot();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 87 || toMorse == 119){
    dot();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 88 || toMorse == 120){
    longg();
    dot();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 89|| toMorse == 121){
    longg();
    dot();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 90 || toMorse == 122){
    longg();
    longg();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }
// цифри
  if(toMorse == 48){ //0
    longg();
    longg();
    longg();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 49){ //1
    dot();
    longg();
    longg();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 50){ //2
    dot();
    dot();
    longg();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 51){ //3
    dot();
    dot();
    dot();
    longg();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 52){ //4
    dot();
    dot();
    dot();
    dot();
    longg();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 53){ //5
    dot();
    dot();
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 54){ //6
    longg();
    dot();
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 55){ //7
    longg();
    longg();
    dot();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 56){ //8
    longg();
    longg();
    longg();
    dot();
    dot();
    SBL();
    serialprnt(toMorse);
  }

  if(toMorse == 57){ //9
    longg();
    longg();
    longg();
    longg();
    dot();
    SBL();
    serialprnt(toMorse);
  }


}

void serialprnt(char input) {
  Serial.print(input);
}

void longg(){
  digitalWrite(led, HIGH);
  delay(300);
  digitalWrite(led, LOW);
  SBL();
}

void dot(){
  digitalWrite(led, HIGH);
  delay(100);
  digitalWrite(led, LOW);
  SBL();
}

void SBL(){
  delay(100);
}
void spaceBtwWords() {
  delay(700);
}

void loop() {
  if (Serial.available()>0) {
    char input = Serial.read();
    Serial.print(input);
    morse(input);
  }
}
